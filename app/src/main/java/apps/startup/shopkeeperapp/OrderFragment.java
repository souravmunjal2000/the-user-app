package apps.startup.shopkeeperapp;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import apps.startup.shopkeeperapp.adapter.PreviousOrderListAdapter;
import apps.startup.shopkeeperapp.application.SharedPrefHelper;
import apps.startup.shopkeeperapp.base.BaseFragment;
import apps.startup.shopkeeperapp.databinding.FragmentOrderBinding;
import apps.startup.shopkeeperapp.model.Order;

import static apps.startup.shopkeeperapp.Constants.ORDER_DATABASE_NAME;

public class OrderFragment extends BaseFragment {
    FragmentOrderBinding binding;
    DatabaseReference reference;
    ProgressDialog progressDialog;
    ArrayList<Order> orders=new ArrayList<>();
    String userid= FirebaseAuth.getInstance().getCurrentUser().getUid();
    PreviousOrderListAdapter previousOrderListAdapter;
    @Override
    public int setViewId() {
        return R.layout.fragment_order;
    }

    @Override
    public void onFragmentCreated() {
        previousOrderListAdapter =new PreviousOrderListAdapter(orders,getContext(),getActivity().getSupportFragmentManager());
        progressDialog=new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        reference= FirebaseDatabase.getInstance().getReference().child(ORDER_DATABASE_NAME).child(userid);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                orders.clear();
                progressDialog.dismiss();
                for(DataSnapshot d :dataSnapshot.getChildren())
                {
                    binding.error.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    orders.add(d.getValue(Order.class));
                    previousOrderListAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        binding.rvOrderList.setAdapter(previousOrderListAdapter);
        binding.rvOrderList.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void bindView(View view) {
        binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {

    }
}
