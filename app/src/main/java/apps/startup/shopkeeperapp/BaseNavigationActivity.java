package apps.startup.shopkeeperapp;

import android.os.Bundle;

import apps.startup.shopkeeperapp.base.BaseActivity;

import static apps.startup.shopkeeperapp.Constants.ATTACH_DOCUMENTS_FRAGMENT;
import static apps.startup.shopkeeperapp.Constants.FRAGMENT_NAME;
import static apps.startup.shopkeeperapp.Constants.ORDER_HAPPY_COMPLETED_FRAGMENT;
import static apps.startup.shopkeeperapp.Constants.PAYMENT_FRAGMENT;
import static apps.startup.shopkeeperapp.Constants.PAYMENT_SUCCESS_FRAGMENT;
import static apps.startup.shopkeeperapp.Constants.PAYMENT_UNSUCCESS_FRAGMENT;
import static apps.startup.shopkeeperapp.Constants.PROFILE_FRAGMENT;
import static apps.startup.shopkeeperapp.Constants.RECEIVING_FRAGMENT;
import static apps.startup.shopkeeperapp.Constants.SHOP_DETAILS_FRAGMENT;

public class BaseNavigationActivity extends BaseActivity {
    FragmentController fragmentController=new FragmentController();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_navigation);
        String fragmentName=getIntent().getExtras().getString(FRAGMENT_NAME);
        if(fragmentName!=null)
        {
            switch (fragmentName) {
                case SHOP_DETAILS_FRAGMENT:
                    setTitle("Shop");
                    ShopDetailsFragment shopDetailsFragment=new ShopDetailsFragment();
                    fragmentController.loadFragment(shopDetailsFragment,getSupportFragmentManager(),R.id.base_navigation_layout,false);
                    break;
                case ATTACH_DOCUMENTS_FRAGMENT:
                    setTitle("Attach Documents");
                    AttachDocumentsFragment attachDocumentsFragment=new AttachDocumentsFragment();
                    fragmentController.loadFragment(attachDocumentsFragment,getSupportFragmentManager(),R.id.base_navigation_layout,false);
                    break;
                case ORDER_HAPPY_COMPLETED_FRAGMENT:
                    setTitle("Order Completed");
                    HappyOrderCompletedFragment happyOrderCompletedFragment=new HappyOrderCompletedFragment();
                    fragmentController.loadFragment(happyOrderCompletedFragment,getSupportFragmentManager(),R.id.base_navigation_layout,false);
                    break;
                case PROFILE_FRAGMENT:
                    setTitle("User Profile");
                    ProfileFragment profileFragment=new ProfileFragment();
                    fragmentController.loadFragment(profileFragment,getSupportFragmentManager(),R.id.base_navigation_layout,false);
                    break;
                case PAYMENT_SUCCESS_FRAGMENT:
                    setTitle("Payment Successful");
                    PaymentPaidSuccessFullFragment paymentPaidSuccessFullFragment=new PaymentPaidSuccessFullFragment();
                    fragmentController.loadFragment(paymentPaidSuccessFullFragment,getSupportFragmentManager(),R.id.base_navigation_layout,false);
                    break;
                case PAYMENT_UNSUCCESS_FRAGMENT:
                    setTitle("Payment Failed");
                    PaymentUnsuccessfullFragment paymentUnsuccessfullFragment=new PaymentUnsuccessfullFragment();
                    fragmentController.loadFragment(paymentUnsuccessfullFragment,getSupportFragmentManager(),R.id.base_navigation_layout,false);
                    break;
            }
        }
    }

    @Override
    public boolean showBackButton() {
        return true;
    }
}
