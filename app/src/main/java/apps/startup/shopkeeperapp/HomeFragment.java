package apps.startup.shopkeeperapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import apps.startup.shopkeeperapp.adapter.ShopsAdapter;
import apps.startup.shopkeeperapp.application.SharedPrefHelper;
import apps.startup.shopkeeperapp.base.BaseFragment;
import apps.startup.shopkeeperapp.databinding.FragmentHomeBinding;
import apps.startup.shopkeeperapp.model.AddShops;

import static apps.startup.shopkeeperapp.Constants.FRAGMENT_NAME;
import static apps.startup.shopkeeperapp.Constants.IS_LOGGED_IN;
import static apps.startup.shopkeeperapp.Constants.SHOP_DATABASE_NAME;
import static apps.startup.shopkeeperapp.Constants.SHOP_DETAILS_FRAGMENT;


public class HomeFragment extends BaseFragment {
    FragmentHomeBinding binding;
    ShopsAdapter shopsAdapter;
    DatabaseReference firebaseDatabase;
    ArrayList<AddShops> addShops=new ArrayList<>();
    ProgressDialog progressDialog;
    ArrayList<String> shopowners=new ArrayList<>();
    @Override
    public int setViewId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onFragmentCreated() {
        progressDialog=new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        firebaseDatabase= FirebaseDatabase.getInstance().getReference().child(SHOP_DATABASE_NAME);
        setSpinnerData();
        firebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                addShops.clear();
                shopowners.clear();
                shopsAdapter.notifyDataSetChanged();
                for(DataSnapshot d :dataSnapshot.getChildren())
                {
                    shopowners.add(d.getKey());
                }
                showShops();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        shopsAdapter=new ShopsAdapter(addShops, new Callback() {
            @Override
            public void onEventDone(Object object) {
                Intent i =new Intent(getActivity(),BaseNavigationActivity.class);
                Bundle bundle=new Bundle();
                bundle.putString(FRAGMENT_NAME,SHOP_DETAILS_FRAGMENT);
                bundle.putSerializable("shop",(AddShops)object);
                i.putExtras(bundle);
                startActivity(i);
            }
        });
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        binding.recyclerView.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        binding.recyclerView.setAdapter(shopsAdapter);
    }

    private void setSpinnerData() {
        Spinner spinner=binding.spinner;
        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = ArrayAdapter.createFromResource(getContext(),
                R.array.areas,
                R.layout.spinner_item_layout);
        aa.setDropDownViewResource(R.layout.dropdown_spinner_layout);
        //Setting the ArrayAdapter data on the Spinner
        spinner.setAdapter(aa);
    }

    public void showShops()
    {
        for (String s:shopowners)
        {
            DatabaseReference firebaseDatabase= FirebaseDatabase.getInstance().getReference().child(SHOP_DATABASE_NAME).child(s);
            firebaseDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    progressDialog.dismiss();
                    for(DataSnapshot d :dataSnapshot.getChildren())
                    {
                        addShops.add(d.getValue(AddShops.class));
                        shopsAdapter.notifyDataSetChanged();
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    @Override
    public void bindView(View view) {
        binding=DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory () {

    }
}
