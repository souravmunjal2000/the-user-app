package apps.startup.shopkeeperapp.Utils;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import java.util.HashMap;
import java.util.Map;
import apps.startup.shopkeeperapp.Constants;
import apps.startup.shopkeeperapp.model.Checksum;
import apps.startup.shopkeeperapp.model.Paytm;
import apps.startup.shopkeeperapp.network.PaytmApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PaytmUtils implements PaytmPaymentTransactionCallback {
    String totalAmount;
    Context context;
    apps.startup.shopkeeperapp.Callback callback;

    public PaytmUtils(Context context, String amount, apps.startup.shopkeeperapp.Callback callback){
        this.context=context;
        totalAmount=amount;
        this.callback=callback;
    }

    public void generateCheckSum() {

        //getting the tax amount first.
        String txnAmount = totalAmount;

        //creating a retrofit object.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(PaytmApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //creating the retrofit api service
        PaytmApi apiService = retrofit.create(PaytmApi.class);

        //creating paytm object
        //containing all the values required
        final Paytm paytm = new Paytm(
                Constants.M_ID,
                Constants.CHANNEL_ID,
                txnAmount,
                Constants.WEBSITE,
                Constants.CALLBACK_URL,
                Constants.INDUSTRY_TYPE_ID
        );

        //creating a call object from the apiService
        Call<Checksum> call = apiService.getChecksum(
                paytm.getmId(),
                paytm.getOrderId(),
                paytm.getCustId(),
                paytm.getChannelId(),
                paytm.getTxnAmount(),
                paytm.getWebsite(),
                paytm.getCallBackUrl(),
                paytm.getIndustryTypeId()
        );

        //making the call to generate checksum
        call.enqueue(new Callback<Checksum>() {
            @Override
            public void onResponse(Call<Checksum> call, Response<Checksum> response) {

                //once we get the checksum we will initiailize the payment.
                //the method is taking the checksum we got and the paytm object as the parameter
                initializePaytmPayment(response.body().getChecksumHash(), paytm);
            }

            @Override
            public void onFailure(Call<Checksum> call, Throwable t) {

            }
        });
    }

    private void initializePaytmPayment(String checksumHash, Paytm paytm) {

        //getting paytm service
        //PaytmPGService Service = PaytmPGService.getStagingService();

        //use this when using for production
        PaytmPGService Service = PaytmPGService.getProductionService();

        //creating a hashmap and adding all the values required
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("MID", Constants.M_ID);
        paramMap.put("ORDER_ID", paytm.getOrderId());
        paramMap.put("CUST_ID", paytm.getCustId());
        paramMap.put("CHANNEL_ID", paytm.getChannelId());
        paramMap.put("TXN_AMOUNT", paytm.getTxnAmount());
        paramMap.put("WEBSITE", paytm.getWebsite());
        paramMap.put("CALLBACK_URL", paytm.getCallBackUrl());
        paramMap.put("CHECKSUMHASH", checksumHash);
        paramMap.put("INDUSTRY_TYPE_ID", paytm.getIndustryTypeId());


        //creating a paytm order object using the hashmap
        PaytmOrder order = new PaytmOrder((HashMap<String, String>) paramMap);

        //intializing the paytm service
        Service.initialize(order, null);

        //finally starting the payment transaction
        Service.startPaymentTransaction(context, true, true, this);

    }

    //all these overriden method is to detect the payment result accordingly
    @Override
    public void onTransactionResponse(Bundle bundle) {
        callback.onEventDone(bundle.getString("STATUS"));
    }

    @Override
    public void networkNotAvailable() {
        callback.onEventDone("Network error");
    }

    @Override
    public void clientAuthenticationFailed(String s) {
        callback.onEventDone(s);
    }

    @Override
    public void someUIErrorOccurred(String s) {
        callback.onEventDone(s);
    }

    @Override
    public void onErrorLoadingWebPage(int i, String s, String s1) {
        callback.onEventDone(s);
    }

    @Override
    public void onBackPressedCancelTransaction() {
        callback.onEventDone("Back Pressed");
    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {
        callback.onEventDone(s+bundle.toString());
    }
}
