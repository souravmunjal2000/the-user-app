package apps.startup.shopkeeperapp;

import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import apps.startup.shopkeeperapp.base.BaseDialogFragment;
import apps.startup.shopkeeperapp.databinding.DialogGetInTouchBinding;

import static android.os.Build.ID;

public class GetInTouchDialog extends BaseDialogFragment {
    DialogGetInTouchBinding binding;
    public static GetInTouchDialog newInstance() {
        GetInTouchDialog getInTouchDialog = new GetInTouchDialog();
        return getInTouchDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final View dialogView = getActivity().getLayoutInflater()
                .inflate(R.layout.dialog_get_in_touch, new LinearLayout(getActivity()),
                        false);
        binding= DataBindingUtil.bind(dialogView);

        binding.callUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               callUs();
            }
        });

        binding.writeEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeEmail();
            }
        });

        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (builder.getWindow() != null) {
            builder.getWindow()
                    .setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        builder.setContentView(dialogView);
        return builder;
    }

    private void callUs() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+"9315164668"));
        startActivity(intent);
    }


    private void writeEmail() {
        String[] TO = {"souravmunjal2000@gmail.com"};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");

        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Payment Failed But Money Got Deducted");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Payment Failed but Money Got Deducted .Please check it Out");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            Log.i("Finished", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getContext(),
                    "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

}
