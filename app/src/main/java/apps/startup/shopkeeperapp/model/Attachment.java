package apps.startup.shopkeeperapp.model;

import android.net.Uri;

import java.io.Serializable;

public class Attachment implements Serializable {
    String file_name;
    Uri file_path;

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public Uri getFile_path() {
        return file_path;
    }

    public void setFile_path(Uri file_path) {
        this.file_path = file_path;
    }
}
