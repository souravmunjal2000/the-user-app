package apps.startup.shopkeeperapp.model;

import java.io.Serializable;

public class Facilitiesandprices implements Serializable {
    String prices;
    String facilities;
    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public String getPrices() {
        return prices;
    }

    public void setPrices(String prices) {
        this.prices = prices;
    }
}

