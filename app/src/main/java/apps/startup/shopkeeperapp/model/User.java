package apps.startup.shopkeeperapp.model;

import java.io.Serializable;

public class User implements Serializable {
    String name;
    String email;
    String phoneno;
    public User() {

    }
    public User(String name,String email) {
        this.name = name;
        this.phoneno ="Null";
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
