package apps.startup.shopkeeperapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SMSBody  implements Serializable {
    @SerializedName("phone_number")
    String phoneNumber;

    @SerializedName("message")
    String message;

    @SerializedName("device_id")
    int deviceId;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }
}
