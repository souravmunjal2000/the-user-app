package apps.startup.shopkeeperapp.model;

import java.io.Serializable;
import java.util.ArrayList;

public class AddShops implements Serializable {
    String name;
    ArrayList<Facilitiesandprices> what_shops_do;
    String phoneno;
    float rating;
    String userid;
    String whatshopsdo;
    String photourl;
    String address;
    double lat=0,longi=0;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLongi() {
        return longi;
    }

    public void setLongi(double longi) {
        this.longi = longi;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhotourl() {
        return photourl;
    }

    public void setPhotourl(String photourl) {
        this.photourl = photourl;
    }

    public String getWhatshopsdo() {
        return whatshopsdo;
    }

    public void setWhatshopsdo(String whatshopsdo) {
        this.whatshopsdo = whatshopsdo;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Facilitiesandprices> getWhat_shops_do() {
        return what_shops_do;
    }

    public void setWhat_shops_do(ArrayList<Facilitiesandprices> what_shops_do) {
        this.what_shops_do = what_shops_do;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
