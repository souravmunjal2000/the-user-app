package apps.startup.shopkeeperapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import static apps.startup.shopkeeperapp.Constants.FRAGMENT_NAME;
import static apps.startup.shopkeeperapp.Constants.PROFILE_FRAGMENT;
import static apps.startup.shopkeeperapp.Constants.REQEST_PERMISSION_CODE;

public class LandingActivity extends AppCompatActivity {
    FragmentController fragmentController=new FragmentController();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.homeFragment:
                    setTitle("Home");
                    HomeFragment homeFragment=new HomeFragment();
                    fragmentController.loadFragment(homeFragment,getSupportFragmentManager(),R.id.landing_fragment_navigation,false);
                    return true;
                case R.id.orderFragment:
                    setTitle("Your Orders");
                    OrderFragment orderFragment=new OrderFragment();
                    fragmentController.loadFragment(orderFragment,getSupportFragmentManager(),R.id.landing_fragment_navigation,false);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        loadHomeFragment();
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        loadHomeFragment();
    }

    public void loadHomeFragment()
    {
        setTitle("Home");
        FragmentController fragmentController=new FragmentController();
        HomeFragment fragment=new HomeFragment();
        fragmentController.loadFragment(fragment,getSupportFragmentManager(),R.id.landing_fragment_navigation,false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e("tagb ", "onRequestPermissionsResult: ");
        if(requestCode == REQEST_PERMISSION_CODE) {
            boolean allPermissionsGranted = true;
            Log.e("tab",""+permissions.length+grantResults.length);
            for(int result : grantResults) {
                if(result != 0) {
                    allPermissionsGranted = false;
                }
            }

            if(!allPermissionsGranted) {
                finish();
            }else{

            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.user, menu);
        // return true so that the menu pop up is opened
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.log_out) {
            Intent i =new Intent(this, BaseNavigationActivity.class);
            Bundle bundle=new Bundle();
            bundle.putString(FRAGMENT_NAME,PROFILE_FRAGMENT);
            i.putExtras(bundle);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
