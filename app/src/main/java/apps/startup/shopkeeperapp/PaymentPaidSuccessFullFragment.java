package apps.startup.shopkeeperapp;

import android.databinding.DataBindingUtil;
import android.view.View;

import apps.startup.shopkeeperapp.base.BaseFragment;
import apps.startup.shopkeeperapp.databinding.FragmentPaymentPaidSuccessBinding;
import apps.startup.shopkeeperapp.databinding.FragmentPaymentUnsuccessfulBinding;

public class PaymentPaidSuccessFullFragment extends BaseFragment {
    FragmentPaymentPaidSuccessBinding binding;
    @Override
    public int setViewId() {
        return R.layout.fragment_payment_paid_success;
    }

    @Override
    public void onFragmentCreated() {
        binding.done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }

    @Override
    public void bindView(View view) {
        binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {

    }
}
