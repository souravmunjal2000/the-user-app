package apps.startup.shopkeeperapp.OnBoardingScreens;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

import apps.startup.shopkeeperapp.Login.LoginActivity;
import apps.startup.shopkeeperapp.R;
import apps.startup.shopkeeperapp.adapter.PhotoAdapter;
import apps.startup.shopkeeperapp.application.SharedPrefHelper;
import apps.startup.shopkeeperapp.base.BaseActivity;
import apps.startup.shopkeeperapp.databinding.ActivityOnBoardingBinding;

import static apps.startup.shopkeeperapp.Constants.IS_DONE_TUTORIAL;

public class OnBoardingActivity extends BaseActivity {
    ActivityOnBoardingBinding binding;
    boolean goToLogin=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_on_boarding);
        //Make list of fragment layouts to be show in view pager
        ArrayList<Fragment> fragmentsName=new ArrayList<>();
        fragmentsName.add(new ShowTutorialAFragment());
        fragmentsName.add(new ShowTutorialBFragment());
        fragmentsName.add(new ShowTutorialCFragment());

        ViewPager pager =binding.photosViewpager;
        PhotoAdapter adapter = new PhotoAdapter(getSupportFragmentManager(),fragmentsName);
        pager.setAdapter(adapter);

        //This is to disable swiping
        pager.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });

        //to skip the tutorial
        binding.skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OnBoardingActivity.this, LoginActivity.class);
                startActivity(i);
                SharedPrefHelper.getInstance().setBooleanData(IS_DONE_TUTORIAL,true);
            }
        });

        binding.next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!goToLogin) {
                    if (pager.getCurrentItem() == 1) {
                        pager.setCurrentItem(pager.getCurrentItem() + 1);
                        binding.next.setText("DONE");
                        goToLogin=true;
                    } else {
                        pager.setCurrentItem(pager.getCurrentItem() + 1);
                    }
                }else{
                    Intent i = new Intent(OnBoardingActivity.this, LoginActivity.class);
                    startActivity(i);
                    SharedPrefHelper.getInstance().setBooleanData(IS_DONE_TUTORIAL,true);
                }
            }
        });

        TabLayout tabLayout =binding.tabLayout;
        tabLayout.setupWithViewPager(pager, true);
    }

    @Override
    public boolean showBackButton() {
        return false;
    }

}
