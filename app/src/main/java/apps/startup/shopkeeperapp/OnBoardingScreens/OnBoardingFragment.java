package apps.startup.shopkeeperapp.OnBoardingScreens;

import android.databinding.DataBindingUtil;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import java.util.ArrayList;

import apps.startup.shopkeeperapp.R;
import apps.startup.shopkeeperapp.adapter.PhotoAdapter;
import apps.startup.shopkeeperapp.base.BaseFragment;
import apps.startup.shopkeeperapp.databinding.ActivityOnBoardingBinding;

public class OnBoardingFragment extends BaseFragment {
    ActivityOnBoardingBinding binding;
    @Override
    public int setViewId() {
        return R.layout.activity_on_boarding;
    }

    @Override
    public void onFragmentCreated() {
//        ArrayList<Fragment> fragments=new ArrayList<>();
//        fragments.add(new ShowTutorialAFragment());
//        fragments.add(new ShowTutorialAFragment());
//
//        ViewPager pager =binding.photosViewpager;
//        PhotoAdapter adapter = new PhotoAdapter(fragments,getContext());
//        pager.setAdapter(adapter);
//        TabLayout tabLayout =binding.tabLayout;
//        tabLayout.setupWithViewPager(pager, true);
    }

    @Override
    public void bindView(View view) {
    binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {

    }
}
