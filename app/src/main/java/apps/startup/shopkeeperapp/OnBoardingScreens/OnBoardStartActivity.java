package apps.startup.shopkeeperapp.OnBoardingScreens;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import apps.startup.shopkeeperapp.R;
import apps.startup.shopkeeperapp.base.BaseActivity;

public class OnBoardStartActivity extends BaseActivity {
    @Override
    public boolean showBackButton() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_board_start);
        Button button= findViewById(R.id.lets_get_started);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(OnBoardStartActivity.this,OnBoardingActivity.class);
                startActivity(intent);
            }
        });
    }


}
