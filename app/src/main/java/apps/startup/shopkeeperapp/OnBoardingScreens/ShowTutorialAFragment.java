package apps.startup.shopkeeperapp.OnBoardingScreens;

import android.view.View;

import apps.startup.shopkeeperapp.R;
import apps.startup.shopkeeperapp.base.BaseFragment;

public class ShowTutorialAFragment extends BaseFragment {
    @Override
    public int setViewId() {
        return R.layout.fragment_show_tutorial_a;
    }

    @Override
    public void onFragmentCreated() {

    }

    @Override
    public void bindView(View view) {

    }

    @Override
    public void getComponentFactory() {

    }
}
