package apps.startup.shopkeeperapp;

import android.databinding.DataBindingUtil;
import android.view.View;

import apps.startup.shopkeeperapp.base.BaseFragment;
import apps.startup.shopkeeperapp.databinding.FragmentPaymentUnsuccessfulBinding;

public class PaymentUnsuccessfullFragment extends BaseFragment {
    FragmentPaymentUnsuccessfulBinding binding;
    @Override
    public int setViewId() {
        return R.layout.fragment_payment_unsuccessful;
    }

    @Override
    public void onFragmentCreated() {
        binding.done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        binding.getInTouch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetInTouchDialog getInTouchDialog = GetInTouchDialog.newInstance();
                getInTouchDialog.show(getActivity().getSupportFragmentManager(), getClass().getSimpleName());
            }
        });
    }

    @Override
    public void bindView(View view) {
        binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {

    }
}
