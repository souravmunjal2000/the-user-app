package apps.startup.shopkeeperapp;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import apps.startup.shopkeeperapp.application.PrintApplication;

public class Toaster {
    public static void makeToast(String string) {
        Toast.makeText(PrintApplication.getInstance().getBaseContext(), ""+string, Toast.LENGTH_SHORT).show();
    }

    public static void makeSnackBar(View view,String message)
    {
        final Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        snackbar.setAction("Dismiss", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

}
