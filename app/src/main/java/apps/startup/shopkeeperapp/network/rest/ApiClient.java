package apps.startup.shopkeeperapp.network.rest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {
    private static Retrofit retrofit = null;
    private static final String BASE_URL = "https://smsgateway.me/";
    public static Retrofit getClient() {
        if (retrofit==null) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder().connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100,TimeUnit.SECONDS).writeTimeout(100, TimeUnit.SECONDS);

            httpClient.addInterceptor(chain -> {
                Request request = chain.request().newBuilder()
                        .addHeader("Authorization","eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTU0OTg5OTI2MSwiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjY3OTI1LCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.ZB9rS_bL2ZTbOvJkw4JE35tjcK8duJXAlpXT74MWcow").build();
                return chain.proceed(request);

            });
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL).client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}

