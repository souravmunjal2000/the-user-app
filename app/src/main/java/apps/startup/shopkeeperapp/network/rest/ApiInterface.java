package apps.startup.shopkeeperapp.network.rest;


import java.util.List;
import apps.startup.shopkeeperapp.model.SMSBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInterface {
    @POST("api/v4/message/send")
    Call<ResponseBody> sendSms(@Body List<SMSBody> body);
}