package apps.startup.shopkeeperapp;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.RatingBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import apps.startup.shopkeeperapp.Login.LoginActivity;
import apps.startup.shopkeeperapp.application.SharedPrefHelper;
import apps.startup.shopkeeperapp.base.BaseFragment;
import apps.startup.shopkeeperapp.databinding.FragmentProfileBinding;
import apps.startup.shopkeeperapp.model.User;

import static apps.startup.shopkeeperapp.Constants.APP_URL;
import static apps.startup.shopkeeperapp.Constants.USER_DATABASE_NAME;
import static apps.startup.shopkeeperapp.Constants.IS_LOGGED_IN;


public class ProfileFragment extends BaseFragment {
    FragmentProfileBinding binding;
    DatabaseReference firebaseDatabase;
    String userid= FirebaseAuth.getInstance().getCurrentUser().getUid();
    ValueEventListener postListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            User user_data = dataSnapshot.getValue(User.class);
            binding.setUserdata(user_data);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.w("LandingActivity", "loadPost:onCancelled", databaseError.toException());
        }
    };
    @Override
    public int setViewId() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onFragmentCreated() {
        binding.ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(APP_URL)));
            }
        });
        binding.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
                FirebaseAuth.getInstance().signOut();
                Intent i=new Intent(getContext(), LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                SharedPrefHelper.getInstance().setBooleanData(IS_LOGGED_IN,false);
                startActivity(i);
            }
        });
        firebaseDatabase= FirebaseDatabase.getInstance().getReference();
        firebaseDatabase.child(USER_DATABASE_NAME).child(userid).addValueEventListener(postListener);

    }




    @Override
    public void bindView(View view) {
        binding=DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {
    }

}
