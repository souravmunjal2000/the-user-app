package apps.startup.shopkeeperapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import apps.startup.shopkeeperapp.Utils.FileUtils;
import apps.startup.shopkeeperapp.Utils.ImageUtils;
import apps.startup.shopkeeperapp.Utils.SMSUtils;
import apps.startup.shopkeeperapp.adapter.AttachmentAdapter;
import apps.startup.shopkeeperapp.application.SharedPrefHelper;
import apps.startup.shopkeeperapp.base.BaseFragment;
import apps.startup.shopkeeperapp.databinding.FragmentAttachDocumentsBinding;
import apps.startup.shopkeeperapp.model.Attachment;
import apps.startup.shopkeeperapp.model.Order;
import static apps.startup.shopkeeperapp.Constants.FRAGMENT_NAME;
import static apps.startup.shopkeeperapp.Constants.ORDER_DATABASE_NAME;
import static apps.startup.shopkeeperapp.Constants.ORDER_HAPPY_COMPLETED_FRAGMENT;
import static apps.startup.shopkeeperapp.Constants.ORDER_ID;
import static apps.startup.shopkeeperapp.Constants.PICKFILE_REQUEST_CODE;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


public class AttachDocumentsFragment extends BaseFragment {
    private FragmentAttachDocumentsBinding binding;
    private ArrayList<Attachment> attachments=new ArrayList<>();
    private AttachmentAdapter attachmentAdapter;
    private Order order;
    ProgressDialog progressDialog;
    String dateandtime;
    Spinner spinner;
    StorageReference storageReference;
    UploadTask uploadTask;
    private DatabaseReference mDatabase;

    @Override
    public int setViewId() {
        return R.layout.fragment_attach_documents;
    }

    @Override
    public void onFragmentCreated() {
        progressDialog= new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Uploading...");
        Bundle bundle = getActivity().getIntent().getExtras();
        storageReference= FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        order=(Order) bundle.getSerializable("order");
        dateandtime = String.valueOf((System.currentTimeMillis()/1000));
        attachmentAdapter=new AttachmentAdapter(attachments, new Callback() {
            @Override
            public void onEventDone(Object object) {
                attachments=(ArrayList<Attachment>) object;
            }
        });
        binding.attachDocuments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(attachments.size()<3)
                {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("*/*");
                    startActivityForResult(intent, PICKFILE_REQUEST_CODE);
                }
                else
                {
                    Toast.makeText(getContext(), "Sorry you cannot put more files than this", Toast.LENGTH_SHORT).show();
                }
            }
        });
        binding.proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                order.setDocumentViewingApp(spinner.getSelectedItem().toString());
                if(attachments.size()>0)
                    uploadImage();
                else
                    Toast.makeText(getContext(), "Please select the documents", Toast.LENGTH_SHORT).show();
            }
        });

        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.rvDocument.setAdapter(attachmentAdapter);
        binding.rvDocument.setLayoutManager(linearLayoutManager);
        setUpSpinnerData();
    }

    private void setUpSpinnerData() {
        spinner=binding.spinner;
        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = ArrayAdapter.createFromResource(getContext(),
                R.array.dept,
                R.layout.spinner_item_layout);
        aa.setDropDownViewResource(R.layout.dropdown_spinner_layout);
        //Setting the ArrayAdapter data on the Spinner
        spinner.setAdapter(aa);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==getActivity().RESULT_OK) {
            if (requestCode == PICKFILE_REQUEST_CODE) {
                Uri returnUri = data.getData();
                Toast.makeText(getContext(), ""+getContext().getContentResolver().getType(returnUri), Toast.LENGTH_SHORT).show();
                Attachment attachment = new Attachment();
                attachment.setFile_path(returnUri);
                attachment.setFile_name(getFileName(returnUri));
                attachments.add(attachment);
                attachmentAdapter.notifyDataSetChanged();
            }
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


    public void uploadImage()
    {
        storageReference=storageReference.child(ORDER_DATABASE_NAME).child(FirebaseAuth.getInstance().getUid()).child(dateandtime);
        for(int i =0;i<attachments.size();i++) {
            progressDialog.show();
            final StorageReference temp=storageReference.child("image_"+(i+1));
            uploadTask=temp.putFile(attachments.get(i).getFile_path());
            final int finalI = i;
            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        progressDialog.dismiss();
                        throw task.getException();
                    }
                    // Continue with the task to get the download URL
                    return temp.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        order.getAttachmentsUrl().add(task.getResult().toString()) ;
                        if(finalI==attachments.size()-1)
                            upload();
                    } else {
                        progressDialog.dismiss();
                        // Handle failures
                        // ...
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), ""+e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }
    public void upload()
    {
        order.setDateandtime(dateandtime);
        RadioButton button=getActivity().findViewById(binding.radioGroupDelivery.getCheckedRadioButtonId());
        order.setReceivingMethod(button.getText().toString());
        mDatabase.child(ORDER_DATABASE_NAME).child(FirebaseAuth.getInstance().getUid()).child(dateandtime).setValue(order);
        progressDialog.dismiss();
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity(), R.style.DialogTheme);
        builder.setMessage("Order created successfully")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                SMSUtils.sendSms(order.getShop().getPhoneno().substring(1),"Hey Owner!You have got a new Order!Please Check the app Order id"+order.getDateandtime());
                            }
                        }).start();
                        Intent i =new Intent(getContext(), BaseNavigationActivity.class);
                        Bundle bundle=new Bundle();
                        bundle.putString(FRAGMENT_NAME,ORDER_HAPPY_COMPLETED_FRAGMENT);
                        bundle.putString(ORDER_ID,dateandtime);
                        i.putExtras(bundle);
                        startActivity(i);
                        getActivity().finish();
                    }
                })
                .setCancelable(false)
                .show();
    }





    @Override
    public void bindView(View view) {
        binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {
    }
}
