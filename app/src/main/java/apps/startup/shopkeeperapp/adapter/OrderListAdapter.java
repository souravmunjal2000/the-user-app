package apps.startup.shopkeeperapp.adapter;

import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import apps.startup.shopkeeperapp.R;
import apps.startup.shopkeeperapp.base.BaseAdapter;
import apps.startup.shopkeeperapp.model.FacilitiesPricesNumber;

public class OrderListAdapter extends BaseAdapter {
    ArrayList<FacilitiesPricesNumber> facilitiesPricesNumbers;
    public OrderListAdapter(ArrayList<FacilitiesPricesNumber> facilitiesPricesNumbers)
    {
        this.facilitiesPricesNumbers=facilitiesPricesNumbers;
    }
    @Override
    public Object getDataAtPosition(int position) {
        return facilitiesPricesNumbers.get(position);
    }

    @Override
    public int getLayoutIdForType(int viewType) {
        return R.layout.order_items_layout;
    }

    @Override
    public void onItemClick(Object object, int position) {

    }

    @Override
    public void editHeightWidthItem(View view, ViewGroup parent) {

    }

    @Override
    public int getItemCount() {
        return facilitiesPricesNumbers.size();
    }
}
