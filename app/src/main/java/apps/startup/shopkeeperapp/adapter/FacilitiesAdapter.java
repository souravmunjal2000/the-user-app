package apps.startup.shopkeeperapp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import apps.startup.shopkeeperapp.BR;
import apps.startup.shopkeeperapp.Callback;
import apps.startup.shopkeeperapp.R;
import apps.startup.shopkeeperapp.base.BaseAdapter;
import apps.startup.shopkeeperapp.databinding.FacilitiesItemsLayoutBinding;
import apps.startup.shopkeeperapp.model.FacilitiesPricesNumber;
import apps.startup.shopkeeperapp.model.Facilitiesandprices;

public class FacilitiesAdapter extends  RecyclerView.Adapter<FacilitiesAdapter.MyViewHolder> {
    ArrayList<Facilitiesandprices> facilitiesandprices;
    Callback callback;
    public FacilitiesAdapter(ArrayList<Facilitiesandprices> facilitiesandprices,Callback callback)
    {
        this.facilitiesandprices=facilitiesandprices;
        this.callback=callback;
    }


    public  class MyViewHolder extends RecyclerView.ViewHolder {
        private final FacilitiesItemsLayoutBinding binding;

        public MyViewHolder(final FacilitiesItemsLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int i=Integer.parseInt(binding.number.getText().toString());
                    if(i<=2) {
                        i=i+1;
                        Facilitiesandprices facilitiesandprices = (Facilitiesandprices)getDataAtPosition(getAdapterPosition());
                        binding.number.setText(""+(i));
                        FacilitiesPricesNumber facilitiesPricesNumber = new FacilitiesPricesNumber();
                        facilitiesPricesNumber.setFacilities(facilitiesandprices.getFacilities());
                        facilitiesPricesNumber.setPrices(Integer.parseInt(facilitiesandprices.getPrices().substring(1)));
                        facilitiesPricesNumber.setNumbers(i);
                        onItemClick(facilitiesPricesNumber);
                    }
                }
            });
            binding.minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int i=Integer.parseInt(binding.number.getText().toString());
                    if(i>0){
                        i=i-1;
                        binding.number.setText(""+(i));
                        Facilitiesandprices facilitiesandprices = (Facilitiesandprices)getDataAtPosition(getAdapterPosition());
                        FacilitiesPricesNumber facilitiesPricesNumber = new FacilitiesPricesNumber();
                        facilitiesPricesNumber.setFacilities(facilitiesandprices.getFacilities());
                        facilitiesPricesNumber.setPrices(Integer.parseInt(facilitiesandprices.getPrices().substring(1)));
                        facilitiesPricesNumber.setNumbers(i);
                        onItemClick(facilitiesPricesNumber);
                    }
                }
            });
        }

        public void bind(Object obj) {

            binding.setVariable(BR.obj, obj);
            binding.executePendingBindings();
        }
    }
    @Override
    public FacilitiesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create repair new view
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        FacilitiesItemsLayoutBinding binding = DataBindingUtil.inflate(layoutInflater,getLayoutIdForType(viewType), parent, false);
        // set the view's size, margins, paddings and layout parameters
        editHeightWidthItem(binding.getRoot(),parent);
        return new FacilitiesAdapter.MyViewHolder(binding);
    }

    // Replace the contents of repair view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(FacilitiesAdapter.MyViewHolder holder, int position) {
        holder.bind(getDataAtPosition(position));
    }

    @Override
    public int getItemCount() {
        return facilitiesandprices.size();
    }

    public  Object getDataAtPosition(int position){ return facilitiesandprices.get(position);}

    public  int getLayoutIdForType(int viewType){ return  R.layout.facilities_items_layout;}

    public  void onItemClick(Object object){callback.onEventDone(object);}

    public  void editHeightWidthItem(View view, ViewGroup parent){}
}
