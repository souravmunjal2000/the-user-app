package apps.startup.shopkeeperapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class PhotoAdapter extends FragmentPagerAdapter {
    private List mFragmentList = new ArrayList<>();

    public PhotoAdapter(FragmentManager manager,ArrayList<Fragment> fragmentArrayList) {
        super(manager);
        this.mFragmentList=fragmentArrayList;
    }

    @Override
    public Fragment getItem(int position) {
        return (Fragment) mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFrag(Fragment fragment, String title) {
        mFragmentList.add(fragment);
    }


}
