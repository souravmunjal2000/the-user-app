package apps.startup.shopkeeperapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import apps.startup.shopkeeperapp.Callback;
import apps.startup.shopkeeperapp.R;
import apps.startup.shopkeeperapp.base.BaseAdapter;
import apps.startup.shopkeeperapp.model.Attachment;

public class AttachmentAdapter extends BaseAdapter {
    ArrayList<Attachment> data;
    private Callback callback;
    // Provide repair suitable constructor (depends on the kind of dataset)
    public AttachmentAdapter(ArrayList<Attachment> myDataset, Callback callback) {
        data = myDataset;
        this.callback = callback;
    }

    @Override
    public Object getDataAtPosition(int position) {
        return data.get(position);
    }


    @Override
    public int getLayoutIdForType(int viewType) {
        return R.layout.attachment_item_layout;
    }

    @Override
    public void onItemClick(Object object, int position) {
        callback.onEventDone(object);
    }

    @Override
    public void editHeightWidthItem(View view, ViewGroup parent) {}

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        View view = holder.itemView;
        ImageView imageView = view.findViewById(R.id.remove);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.remove(data.get(position));
                onItemClick(data,position);
                notifyDataSetChanged();
            }
        });
    }
}
