package apps.startup.shopkeeperapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import apps.startup.shopkeeperapp.BaseNavigationActivity;
import apps.startup.shopkeeperapp.Callback;
import apps.startup.shopkeeperapp.FullImageDialog;
import apps.startup.shopkeeperapp.GetInTouchDialog;
import apps.startup.shopkeeperapp.R;
import apps.startup.shopkeeperapp.Toaster;
import apps.startup.shopkeeperapp.Utils.PaytmUtils;
import apps.startup.shopkeeperapp.Utils.SMSUtils;
import apps.startup.shopkeeperapp.base.BaseAdapter;
import apps.startup.shopkeeperapp.model.Order;

import static apps.startup.shopkeeperapp.Constants.ACCEPTED;
import static apps.startup.shopkeeperapp.Constants.AMOUNT_TO_BE_PAID;
import static apps.startup.shopkeeperapp.Constants.CANCELLED;
import static apps.startup.shopkeeperapp.Constants.COMPLETED;
import static apps.startup.shopkeeperapp.Constants.DECLINED;
import static apps.startup.shopkeeperapp.Constants.FRAGMENT_NAME;
import static apps.startup.shopkeeperapp.Constants.ORDER_DATABASE_NAME;
import static apps.startup.shopkeeperapp.Constants.PAYMENT_SUCCESS_FRAGMENT;
import static apps.startup.shopkeeperapp.Constants.PAYMENT_UNSUCCESS_FRAGMENT;
import static apps.startup.shopkeeperapp.Constants.SUCCESS;

public class PreviousOrderListAdapter extends BaseAdapter {
    ArrayList<Order> orders;
    Context context;
    FragmentManager fragmentManager;
    public PreviousOrderListAdapter(ArrayList<Order> orders, Context context, FragmentManager fragmentManager)
    {
     this.orders=orders;
     this.context=context;
     this.fragmentManager=fragmentManager;
    }
    @Override
    public Object getDataAtPosition(int position) {
        return orders.get(position);
    }

    @Override
    public int getLayoutIdForType(int viewType) {
        return R.layout.previous_order_item_layout;
    }

    @Override
    public void onItemClick(Object object, int position) {
    }

    @Override
    public void editHeightWidthItem(View view, ViewGroup parent) {
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        View view = holder.itemView;
        if(orders.get(position).getBillStatus().equals(SUCCESS)){
            ImageView paymentstatus=view.findViewById(R.id.payment_status);
            paymentstatus.setVisibility(View.VISIBLE);
        }
        RecyclerView recyclerView = view.findViewById(R.id.rvFacilities);
        OrderListAdapter orderListAdapter =new OrderListAdapter(orders.get(position).getFacilitiesPricesNumbers());
        LinearLayoutManager linearLayoutManage=new LinearLayoutManager(context);
        linearLayoutManage.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setAdapter(orderListAdapter);
        recyclerView.setLayoutManager(linearLayoutManage);


        ImageView settings=view.findViewById(R.id.options);
        //Open menu on setting button click
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(context, settings);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.pop_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if(item.getItemId()==R.id.check_bill_details) {
                            if(orders.get(position).getBillPhoto()!=null){
                                FullImageDialog fullImageDialog = FullImageDialog.newInstance(orders.get(position).getBillPhoto());
                                fullImageDialog.show(fragmentManager,getClass().getSimpleName());
                            }else{
                                Toaster.makeSnackBar(view,"Bill not Updated yet");
                            }

                        }else if(item.getItemId()==R.id.cancel_order){
                            String status=orders.get(position).getStatus();
                            if (status.equals(ACCEPTED)){
                                Toaster.makeSnackBar(view,"Sorry The order has been accepted");
                            }
                            else if (status.equals(COMPLETED)){
                                Toaster.makeSnackBar(view,"Sorry The order has been completed");
                            }
                            else if(status.equals(DECLINED)){
                                Toaster.makeSnackBar(view,"Sorry The order has been declined");
                            }
                            else{
                                Toaster.makeSnackBar(view,"cancelled");
                                orders.get(position).setStatus(CANCELLED);
                                //Update the value
                                updateTheValue(orders.get(position));
                            }
                        }
                        //Once the bill is Paid then message is sent to shop owner
                        else if(item.getItemId()==R.id.pay_bill) {
                            if (!orders.get(position).getBillStatus().equals(SUCCESS)) {
                                if (orders.get(position).getTotalPrice() != 0) {
                                    PaytmUtils paytmUtils = new PaytmUtils(context, String.valueOf(orders.get(position).getTotalPrice()), new Callback() {
                                        @Override
                                        public void onEventDone(Object object) {
                                            String status = (String) object;
                                            if (status.equals("TXN_SUCCESS")) {
                                                orders.get(position).setBillStatus(SUCCESS);
                                                updateTheValue(orders.get(position));
                                                //Show the payment successful fragment
                                                Intent i = new Intent(context, BaseNavigationActivity.class);
                                                Bundle bundle=new Bundle();
                                                bundle.putString(FRAGMENT_NAME,PAYMENT_SUCCESS_FRAGMENT);
                                                i.putExtras(bundle);
                                                context.startActivity(i);
                                                new Thread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        SMSUtils.sendSms(orders.get(position).getShop().getPhoneno().substring(1),"Hey Owner! The bill is paid for order "+orders.get(position).getDateandtime()+"Please Check the app");
                                                    }
                                                }).start();
                                            }
                                            else{
                                                //Show the Payment Unsucessful Fragment
                                                Intent i = new Intent(context, BaseNavigationActivity.class);
                                                Bundle bundle=new Bundle();
                                                bundle.putString(FRAGMENT_NAME,PAYMENT_UNSUCCESS_FRAGMENT);
                                                i.putExtras(bundle);
                                                context.startActivity(i);
                                            }
                                        }
                                    });
                                    paytmUtils.generateCheckSum();
                                } else {
                                    Toaster.makeSnackBar(view, "Bill is not Updated yet");
                                }
                            }
                            else{
                                Toaster.makeSnackBar(view,"You already Paid");
                            }
                        }
                        else if(item.getItemId()==R.id.contact_us){
                            GetInTouchDialog getInTouchDialog = GetInTouchDialog.newInstance();
                            getInTouchDialog.show(fragmentManager, getClass().getSimpleName());
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });

    }


    public void updateTheValue(Order order){
        FirebaseDatabase.getInstance().getReference().child(ORDER_DATABASE_NAME)
                .child(FirebaseAuth.getInstance().getUid()).child(order.getDateandtime())
                .setValue(order);
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }
}
