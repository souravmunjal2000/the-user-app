package apps.startup.shopkeeperapp.Login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;

import apps.startup.shopkeeperapp.LandingActivity;
import apps.startup.shopkeeperapp.OnBoardingScreens.OnBoardStartActivity;
import apps.startup.shopkeeperapp.R;
import apps.startup.shopkeeperapp.application.SharedPrefHelper;
import apps.startup.shopkeeperapp.base.BaseActivity;
import apps.startup.shopkeeperapp.databinding.ActivitySplashBinding;

import static apps.startup.shopkeeperapp.Constants.IS_DONE_TUTORIAL;
import static apps.startup.shopkeeperapp.Constants.IS_LOGGED_IN;


public class Splash extends BaseActivity {
    ActivitySplashBinding activitySplashBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySplashBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    if(!SharedPrefHelper.getInstance().getBooleanData(IS_DONE_TUTORIAL)) {
                        Intent intent = new Intent(Splash.this, OnBoardStartActivity.class);
                        startActivity(intent);
                        finish();
                    } else if(FirebaseAuth.getInstance().getCurrentUser()!=null) {
                        Intent intent = new Intent(Splash.this, LandingActivity.class);
                        startActivity(intent);
                        finish();
                    } else
                    {
                        Intent intent = new Intent(Splash.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    @Override
    public boolean showBackButton() {
        return false;
    }
}

