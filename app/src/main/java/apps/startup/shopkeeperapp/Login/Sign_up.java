package apps.startup.shopkeeperapp.Login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import apps.startup.shopkeeperapp.LandingActivity;
import apps.startup.shopkeeperapp.R;
import apps.startup.shopkeeperapp.Toaster;
import apps.startup.shopkeeperapp.application.SharedPrefHelper;
import apps.startup.shopkeeperapp.base.BaseActivity;
import apps.startup.shopkeeperapp.databinding.ActivitySignUpBinding;
import apps.startup.shopkeeperapp.model.User;

import static apps.startup.shopkeeperapp.Constants.USER_DATABASE_NAME;
import static apps.startup.shopkeeperapp.Constants.IS_LOGGED_IN;
import static apps.startup.shopkeeperapp.Constants.PHONE_NO;


public class Sign_up extends BaseActivity {
    ActivitySignUpBinding binding;
    private FirebaseAuth mAuth;
    private User user=new User();
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        final String phoneNo=getIntent().getExtras().getString(PHONE_NO);
        binding.signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(binding.firstName.getText().toString().equals("")) {
                    Toaster.makeSnackBar(binding.layout,getString(R.string.invalid_name_error));
                    return;
                }
                mDatabase=FirebaseDatabase.getInstance().getReference(USER_DATABASE_NAME).child(mAuth.getCurrentUser().getUid());
                setUserData(binding.firstName.getText().toString()+" "+binding.lastName.getText().toString(),phoneNo);
                mDatabase.setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Toaster.makeSnackBar(binding.layout,getString(R.string.welcome_message));
                                Intent intent=new Intent(Sign_up.this, LandingActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                SharedPrefHelper.getInstance().setBooleanData(IS_LOGGED_IN,true);
                                startActivity(intent);
                            }else{
                                Toaster.makeSnackBar(binding.layout,task.getException().getMessage());
                            }
                        }
                    });
            }

        });

    }

    @Override
    public boolean showBackButton () {
        return true;
    }

    public void setUserData(String name,String phone) {
        user.setName(name);
        user.setPhoneno(phone);
    }


}
