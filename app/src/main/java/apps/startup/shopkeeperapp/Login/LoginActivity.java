package apps.startup.shopkeeperapp.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import apps.startup.shopkeeperapp.FragmentController;
import apps.startup.shopkeeperapp.OnBoardingScreens.OnBoardingFragment;
import apps.startup.shopkeeperapp.R;
import apps.startup.shopkeeperapp.Toaster;
import apps.startup.shopkeeperapp.Utils.BasicUtils;
import apps.startup.shopkeeperapp.databinding.ActivityLoginBinding;

import static apps.startup.shopkeeperapp.Constants.PHONE_NO;


public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LOGIN_ACTIVITY";
    private ActivityLoginBinding binding;
    private ProgressDialog progressDialog;
    private String phoneNo="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.verifying));
        progressDialog.setCancelable(false);

        binding.login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phoneNo=binding.phoneNo.getText().toString();
                if(BasicUtils.isValidMobile(phoneNo)) {
                    Intent intent=new Intent(LoginActivity.this, VerifyOtp.class);
                    Bundle bundle=new Bundle();
                    bundle.putString(PHONE_NO,phoneNo);
                    intent.putExtras(bundle);
                    startActivity(intent);

                }else {
                    Toaster.makeToast(getString(R.string.invalid_phone_error));
                }
            }
        });
    }





}
