package apps.startup.shopkeeperapp.Login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.view.View;

import com.dpizarro.pinview.library.PinView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.concurrent.TimeUnit;
import apps.startup.shopkeeperapp.LandingActivity;
import apps.startup.shopkeeperapp.R;
import apps.startup.shopkeeperapp.Toaster;
import apps.startup.shopkeeperapp.application.SharedPrefHelper;
import apps.startup.shopkeeperapp.base.BaseActivity;
import apps.startup.shopkeeperapp.databinding.ActivityVerifyOtpBinding;
import apps.startup.shopkeeperapp.model.User;

import static apps.startup.shopkeeperapp.Constants.USER_DATABASE_NAME;
import static apps.startup.shopkeeperapp.Constants.IS_LOGGED_IN;
import static apps.startup.shopkeeperapp.Constants.PHONE_NO;

public class VerifyOtp extends BaseActivity {
    ActivityVerifyOtpBinding binding;
    private FirebaseAuth mAuth;
    private String mVerificationId="";
    private String phoneNo;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
//            if (code != null) {
//                verifyVerificationCode(code);
//            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toaster.makeToast(e.getMessage());
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            binding.progressBar.setVisibility(View.INVISIBLE);
            mVerificationId = s;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this, R.layout.activity_verify_otp);
        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();
        phoneNo=getIntent().getExtras().getString(PHONE_NO);
        sendVerificationCode(phoneNo);

        PinView mPinView=binding.pinView;
        mPinView.setPin(6);
        mPinView.setTitles(getResources().getStringArray(R.array.small_titles));
        mPinView.setMaskPassword(false);
        mPinView.setDeleteOnClick(true);
        mPinView.setNativePinBox(true);
        mPinView.setCustomDrawablePinBox(R.drawable.pin_box);
        mPinView.setKeyboardMandatory(false);
        mPinView.setSplit("-");
        mPinView.setColorTitles(Color.rgb(255, 0, 128));
        mPinView.setColorTextPinBoxes(Color.rgb(0, 0, 0));
        mPinView.setColorSplit(Color.rgb(0, 0, 0));

        mPinView.setOnCompleteListener(new PinView.OnCompleteListener() {
            @Override
            public void onComplete(boolean completed, String pinResults) {
                verifyVerificationCode(pinResults);
                binding.progressBar.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    public boolean showBackButton() {
        return true;
    }
    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+91"+mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }

    private void verifyVerificationCode(String otp) {
        //creating the credential
        if(!mVerificationId.equals("")){
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otp);
            //signing the user
            signInWithPhoneAuthCredential(credential);
        }else{
            Toaster.makeSnackBar(findViewById(R.id.parent),getString(R.string.no_verfication_link_sent));
        }


    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            binding.progressBar.setVisibility(View.INVISIBLE);
                            Toaster.makeSnackBar(binding.parent,"DONE");
                            FirebaseUser firebaseUser=FirebaseAuth.getInstance().getCurrentUser();
                            DatabaseReference reference= FirebaseDatabase.getInstance().getReference(USER_DATABASE_NAME).child(firebaseUser.getUid());
                            reference.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    User user=dataSnapshot.getValue(User.class);
                                    if(user==null){
                                        Intent intent=new Intent(VerifyOtp.this, Sign_up.class);
                                        Bundle bundle=new Bundle();
                                        bundle.putString(PHONE_NO,phoneNo);
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                    }else{
                                        Intent intent=new Intent(VerifyOtp.this, LandingActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        SharedPrefHelper.getInstance().setBooleanData(IS_LOGGED_IN,true);
                                        startActivity(intent);
                                    }

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                        else {
                            binding.progressBar.setVisibility(View.INVISIBLE);
                            String message = getString(R.string.something_wrong);
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = getString(R.string.invalid_code);
                            }
                            Toaster.makeSnackBar(binding.parent,message);
                        }
                    }
                });
    }
}
