package apps.startup.shopkeeperapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import static apps.startup.shopkeeperapp.Constants.LATITUDE;
import static apps.startup.shopkeeperapp.Constants.LONGITUDE;
import static apps.startup.shopkeeperapp.Constants.SHOP_ADDRESS;

public class MapsActivity extends AppCompatActivity {
    double latitude,longitude;
    String shopaddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Bundle bundle=getIntent().getExtras();
        latitude=bundle.getDouble(LATITUDE);
        longitude=bundle.getDouble(LONGITUDE);
        shopaddress=bundle.getString(SHOP_ADDRESS);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                LatLng shop = new LatLng(latitude, longitude);
                googleMap.addMarker(new MarkerOptions().position(shop)
                        .title(shopaddress));
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(shop));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
            }
        });
    }
}
