package apps.startup.shopkeeperapp;

public class Constants {

    //-------------Micsellaneous Constants-------------//
    public static final String PRINT_PREFERENCES = "user_print_print_preferences";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final int PICKFILE_REQUEST_CODE=002;
    public static final int REQEST_PERMISSION_CODE=0001;
    public static final String FRAGMENT_NAME="Fragment Name";
    public static final String ATTACH_DOCUMENTS_FRAGMENT="Attach Document Fragment";
    public static final String APP_URL="https://play.google.com/store/apps/details?id=apps.startup.shopkeeperapp";
    public static final String PHONE_NO="phone_no";
    public static final String AMOUNT_TO_BE_PAID="Amount to be paid";
    public static final String IS_DONE_TUTORIAL="done_tutorial";
    public static final String ORDER_ID="order id";
    public static final String LATITUDE="latitude";
    public static final String LONGITUDE="longitude";
    public static final String SHOP_ADDRESS="shop_address";

    //-------------FragmentNames-------------//
    public static final String RECEIVING_FRAGMENT="Receiving Fragment";
    public static final String PAYMENT_FRAGMENT="Payment Fragment";
    public static final String PROFILE_FRAGMENT="profile_fragment";
    public static final String ORDER_HAPPY_COMPLETED_FRAGMENT="Order_happy_completed_fragment";
    public static final String SHOP_DETAILS_FRAGMENT="Shop Details Fragment";
    public static final String PAYMENT_SUCCESS_FRAGMENT="Payment Success Fragment";
    public static final String PAYMENT_UNSUCCESS_FRAGMENT="Payment Unsuccess Fragment";


    //-------------Status Flags-------------//
    public static final String ACCEPTED="Accepted";
    public static final String DECLINED="Declined";
    public static final String COMPLETED="Completed";
    public static final String CANCELLED="cancelled";
    public static final String PROCESSING="Processing";
    public static final String WAITING="waiting";
    public static final String SUCCESS="success";

    //-------------Databases Name-------------//
    public static final String SHOP_DATABASE_NAME="shops";
    public static final String ORDER_DATABASE_NAME="order";
    public static final String USER_DATABASE_NAME = "users";

    //-------------Paytm Creds-------------//
    public static final String M_ID = "LYJjUu09400260361713"; //Paytm Merchand Id we got it in paytm credentials
    public static final String CHANNEL_ID = "WAP"; //Paytm Channel Id, got it in paytm credentials
    public static final String INDUSTRY_TYPE_ID = "Retail"; //Paytm industry type got it in paytm credential
    public static final String WEBSITE = "DEFAULT";
    public static final String CALLBACK_URL = "https://securegw-stage.paytm.in/theia/paytmCallback";

}
