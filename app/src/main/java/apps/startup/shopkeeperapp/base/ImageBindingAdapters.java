package apps.startup.shopkeeperapp.base;

import android.databinding.BindingAdapter;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import apps.startup.shopkeeperapp.R;

import static apps.startup.shopkeeperapp.Constants.ACCEPTED;
import static apps.startup.shopkeeperapp.Constants.CANCELLED;
import static apps.startup.shopkeeperapp.Constants.COMPLETED;
import static apps.startup.shopkeeperapp.Constants.DECLINED;
import static apps.startup.shopkeeperapp.Constants.PROCESSING;
import static apps.startup.shopkeeperapp.Constants.WAITING;


/**
 * Created by prateek on 08/08/18.
 */
public class ImageBindingAdapters {

    @BindingAdapter("imagesetter:imageurl")
    public void setImageUrl(ImageView imageView, String imageurl) {
        Log.e("fsdfs","hey f"+imageurl);
        Picasso.get().load(imageurl).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(imageView);
    }

    @BindingAdapter("half_url")
    public void setImageHalfUrl(ImageView imageView, String url) {
        Picasso.get().load(url).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(imageView);
    }

    @BindingAdapter({"android:src"})
    public void loadImage(ImageView view, int id) {
        Picasso.get().load(id).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(view);
    }

    @BindingAdapter({"android:uri"})
    public void loadImage(ImageView view, Uri id) {
        Picasso.get().load(id).into(view);
    }

    @BindingAdapter({"android:drawable"})
    public void loadImageNoPlacehoalder(ImageView view, int id) {
        Picasso.get().load(id).into(view);
    }

    @BindingAdapter({"imagesetter:setStatus"})
    public void setStatus(TextView view, String status) {
        if(status.equals(PROCESSING)){
            view.setText(WAITING);
        }else if(status.equals(ACCEPTED)){
            view.setText(PROCESSING);
        }else if(status.equals(DECLINED)){
            view.setText(DECLINED);
        }else if(status.equals(CANCELLED)){
            view.setText(CANCELLED);
        }else if (status.equals(COMPLETED)){
            view.setText(COMPLETED);
        }
    }
}