package apps.startup.shopkeeperapp;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import apps.startup.shopkeeperapp.adapter.FacilitiesAdapter;
import apps.startup.shopkeeperapp.adapter.OrderListAdapter;
import apps.startup.shopkeeperapp.base.BaseFragment;
import apps.startup.shopkeeperapp.databinding.FragmentShopDetailsBinding;
import apps.startup.shopkeeperapp.model.AddShops;
import apps.startup.shopkeeperapp.model.Attachment;
import apps.startup.shopkeeperapp.model.FacilitiesPricesNumber;
import apps.startup.shopkeeperapp.model.Facilitiesandprices;
import apps.startup.shopkeeperapp.model.Order;

import static apps.startup.shopkeeperapp.Constants.ATTACH_DOCUMENTS_FRAGMENT;
import static apps.startup.shopkeeperapp.Constants.FRAGMENT_NAME;
import static apps.startup.shopkeeperapp.Constants.LATITUDE;
import static apps.startup.shopkeeperapp.Constants.LONGITUDE;
import static apps.startup.shopkeeperapp.Constants.SHOP_ADDRESS;

public class ShopDetailsFragment extends BaseFragment {
    FragmentShopDetailsBinding binding;
    AddShops shops;
    FacilitiesAdapter facilitiesAdapter;
    Order order=new Order();
    OrderListAdapter orderListAdapter =new OrderListAdapter(order.getFacilitiesPricesNumbers());
    @Override
    public int setViewId() {
        return R.layout.fragment_shop_details;
    }
    @Override
    public void onFragmentCreated() {
        shops =(AddShops) getActivity().getIntent().getExtras().getSerializable("shop");
        if(shops!=null)
            binding.setShop(shops);
        for(Facilitiesandprices facilitiesandprices:shops.getWhat_shops_do()) {
            String prices="₹"+facilitiesandprices.getPrices();
            facilitiesandprices.setPrices(prices);
        }

        binding.showLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(),MapsActivity.class);
                Bundle b = new Bundle();
                b.putString(SHOP_ADDRESS,shops.getAddress());
                b.putDouble(LATITUDE,  shops.getLat());
                b.putDouble(LONGITUDE, shops.getLongi());
                i.putExtras(b);
                startActivity(i);
            }
        });

        binding.proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!order.getFacilitiesPricesNumbers().isEmpty()) {
                    order.setShop(shops);
                    Bundle bundle=new Bundle();
                    bundle.putSerializable("order",order);
                    bundle.putString(FRAGMENT_NAME,ATTACH_DOCUMENTS_FRAGMENT);
                    Intent intent = new Intent(getContext(),BaseNavigationActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);

                }
                else{
                    Toast.makeText(getContext(), "Please add something to place order", Toast.LENGTH_SHORT).show();
                }
            }
        });
        setFacilities(shops.getWhat_shops_do());
    }

    public void setFacilities(final ArrayList<Facilitiesandprices> facilitiesandprices)
    {
        ConstraintLayout layout= binding.bottomSheet;
        final BottomSheetBehavior sheetBehavior=BottomSheetBehavior.from(layout);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        binding.arrow.setImageResource(R.drawable.ic_arrow_downward_black_24dp);
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        binding.arrow.setImageResource(R.drawable.ic_arrow_upward_black_24dp);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        binding.bottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
            }
        });
        facilitiesAdapter=new FacilitiesAdapter(facilitiesandprices, new Callback() {
            @Override
            public void onEventDone(Object object) {
                FacilitiesPricesNumber facilitiesPricesNumber=(FacilitiesPricesNumber) object;
                if(facilitiesPricesNumber.getNumbers()==0)
                {
                    for(FacilitiesPricesNumber allFacilities:order.getFacilitiesPricesNumbers()) {
                        if (allFacilities.getFacilities().equals(facilitiesPricesNumber.getFacilities())){
                            order.getFacilitiesPricesNumbers().remove(allFacilities);
                            orderListAdapter.notifyDataSetChanged();
                            break;
                        }
                    }
                }
                else
                {
                    int found=0;
                    for(FacilitiesPricesNumber allFacilities:order.getFacilitiesPricesNumbers()) {
                        if (allFacilities.getFacilities().equals(facilitiesPricesNumber.getFacilities())){
                            found=1;
                            order.getFacilitiesPricesNumbers().remove(allFacilities);
                            order.getFacilitiesPricesNumbers().add(facilitiesPricesNumber);
                            orderListAdapter.notifyDataSetChanged();
                            break;
                        }
                    }
                    if(found==1)
                    {
                    }
                        else
                    {
                        order.getFacilitiesPricesNumbers().add(facilitiesPricesNumber);
                        orderListAdapter.notifyDataSetChanged();
                    }
                }
            }
        });

        LinearLayoutManager linearLayoutManager1=new LinearLayoutManager(getContext());
        linearLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        LinearLayoutManager linearLayoutManager2=new LinearLayoutManager(getContext());
        linearLayoutManager2.setOrientation(LinearLayoutManager.VERTICAL);
        binding.facilities.setAdapter(facilitiesAdapter);
        binding.facilities.setLayoutManager(linearLayoutManager1);
        binding.orderDetails.setLayoutManager(linearLayoutManager2);
        binding.orderDetails.setAdapter(orderListAdapter);
    }


    @Override
    public void bindView(View view) {
        binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {

    }
}
