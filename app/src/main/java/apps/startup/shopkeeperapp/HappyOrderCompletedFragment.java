package apps.startup.shopkeeperapp;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import apps.startup.shopkeeperapp.R;
import apps.startup.shopkeeperapp.base.BaseFragment;
import apps.startup.shopkeeperapp.databinding.FragmentHappyOrderCompletedBinding;

import static apps.startup.shopkeeperapp.Constants.ORDER_ID;

public class HappyOrderCompletedFragment extends BaseFragment {
    FragmentHappyOrderCompletedBinding binding;
    @Override
    public int setViewId() {
        return R.layout.fragment_happy_order_completed;
    }

    @Override
    public void onFragmentCreated() {
        Bundle bundle = getActivity().getIntent().getExtras();
        String order_id=bundle.getString(ORDER_ID);
        binding.orderId.setText(order_id);
        binding.done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }

    @Override
    public void bindView(View view) {
        binding= DataBindingUtil.bind(view);
    }

    @Override
    public void getComponentFactory() {

    }
}
