package apps.startup.shopkeeperapp.application;

import android.app.Application;
import android.databinding.DataBindingUtil;

import apps.startup.shopkeeperapp.base.MyDataBindingComponent;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;


/**
 * Created by prateek on 16/02/18.
 */
public class PrintApplication extends Application {

    private static PrintApplication instance = null;

    public static PrintApplication getInstance(){
        return instance;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
       // Fabric.with(this, new Crashlytics());
        DataBindingUtil.setDefaultComponent(new MyDataBindingComponent());
    }
}
